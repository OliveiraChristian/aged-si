from django.contrib import admin
from .models import Unit, UnitHierarchy, UnitUser

# Register your models here.
class UnitAdmin(admin.ModelAdmin):
    
    list_display = ['initials', 'hierarchy', 'name', 'address']

    class Meta:
        model = Unit

admin.site.register(Unit, UnitAdmin)

class UnitHierarchyAdmin(admin.ModelAdmin):
    
    list_display = ['get_unit', 'get_superior', ]

    def get_superior(self, obj):
        return obj.superior if obj.superior else ""

    get_superior.short_description = 'Superior Unit'
    get_superior.admin_order_field = 'superior'

    def get_unit(self, obj):
        return obj.unit.name if obj.unit else ""

    get_unit.short_description = 'Unit'
    get_unit.admin_order_field = 'unit'

    class Meta:
        model = UnitHierarchy

admin.site.register(UnitHierarchy, UnitHierarchyAdmin)

class UnitUserAdmin(admin.ModelAdmin):
    
    list_display = ['get_user', 'get_unit', ]

    def get_user(self, obj):
        return obj.user.username

    get_user.short_description = 'User'
    get_user.admin_order_field = 'user'

    def get_unit(self, obj):
        return obj.unit.name if obj.unit else ""

    get_unit.short_description = 'Unit'
    get_unit.admin_order_field = 'unit'

    class Meta:
        model = UnitHierarchy

admin.site.register(UnitUser, UnitUserAdmin)
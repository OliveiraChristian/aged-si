from django.db import models
from addresses.models import Address
from django.contrib.auth import get_user_model

# Create your models here.
User = get_user_model()


class Unit(models.Model):

    HIERARCHY_CHOICES = (
        ('HQ', 'Sede'),
        ('BR', 'Filial'),
        ('DP', 'Setor'),
    )

    hierarchy = models.CharField(max_length=2, choices=HIERARCHY_CHOICES, null=True)
    initials = models.CharField(max_length=10)
    name = models.CharField(max_length=60, unique=True)
    active = models.BooleanField(default=True)
    address = models.OneToOneField(
        Address, on_delete=models.CASCADE, blank=True, null=True)
    obs = models.TextField(blank=True)

    def __str__(self):
        return self.name


class UnitUser(models.Model):

    unit = models.ForeignKey(Unit, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, related_name='units',
                             on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.unit.name if self.unit else ""
        

class UnitHierarchy(models.Model):

    unit = models.OneToOneField(Unit, on_delete=models.SET_NULL, null=True)
    superior = models.ForeignKey(
        Unit, related_name='superior', on_delete=models.SET_NULL, blank=True, null=True)

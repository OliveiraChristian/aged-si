from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^ticket/create$', views.CreateTicket.as_view(), name='create'),
    url(r'^ticket/(?P<pk>\d+)/detail$', views.TicketDetail.as_view(), name='detail'),
    url(r'^ticket/(?P<pk>\d+)/followup$', views.add_follow_up, name='followup'),
    url(r'^ticket/(?P<pk>\d+)/update$', views.TicketUpdate.as_view(), name='update'),
    url(r'^ticket/(?P<pk>\d+)/delete$', views.TicketDelete.as_view(), name='delete'),
    url(r'^ticket/(?P<pk>\d+)/pdf$', views.TicketPDF.as_view(), name='pdf'),
    url(r'^ticket/(?P<pk>\d+)/assing$', views.assign_to, name='assign'),

    url(r'^search/$', views.SearchTicketView.as_view(), name='search'),
    url('^ajax/load-rankings/$', views.load_rankings, name='ajax_load_rankings'),
]
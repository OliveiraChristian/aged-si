from django.db import models
from django.contrib.auth import get_user_model
from django.db.models import Q

# Create your models here.
User = get_user_model()

class Ranking(models.Model):
    """
    Classificacao do chamado
    """

    DEVELOPMENT = 'DE'
    INFRASTRUCTURE = 'IN'
    BUSINESS = 'BN'
    SUPPORT = 'SU'

    IT_QUEUE = (
        (DEVELOPMENT, 'Desenvolvimento'),
        (INFRASTRUCTURE, 'Infraestrutura'),
        (BUSINESS, 'Negócios'),
        (SUPPORT, 'Suporte'),
    )

    name            = models.CharField(max_length=50)
    queue           = models.CharField(max_length=2, choices=IT_QUEUE)
    sla_to_analyze  = models.PositiveSmallIntegerField(blank=True)
    sla_to_solve    = models.PositiveSmallIntegerField(blank=True)

    def __str__(self):
        return self.name


class TicketQuerySet(models.query.QuerySet):

    def user(self, request):
        return self.filter(user=request.user)

    def search(self, query):
        lookups = (
            Q(user__username__icontains=query) |
            Q(pk__icontains=query) |
            Q(ranking__name__icontains=query)
        )
        return self.filter(lookups).distinct()


class TicketManager(models.Manager):

    def get_queryset(self):
        return TicketQuerySet(self.model, using=self._db)

    def search(self, query):
        return self.get_queryset().search(query)


class Ticket(models.Model):
    """
    Ticket do chamado
    """

    OPEN_STATUS = 1
    RESOLVED_STATUS = 2
    CLOSED_STATUS = 3
    REOPENED_STATUS = 4

    STATUS_CHOICES = (
        (OPEN_STATUS, 'Aberto'),
        (RESOLVED_STATUS, 'Solucionado'),
        (CLOSED_STATUS, 'Encerrado'),
        (REOPENED_STATUS, 'Reaberto'),
    )

    PRIORITY_CHOICES = (
        (1, '1 - Alto'),
        (2, '2 - Médio'),
        (3, '3 - Normal'),
        (4, '4 - Baixo'),
    )

    user        = models.ForeignKey(User, related_name='tickets', on_delete=models.SET_NULL, null=True)
    assigned_to = models.ForeignKey(User, related_name='assigned_to', on_delete=models.SET_NULL, null=True)
    ranking     = models.ForeignKey(Ranking, on_delete=models.SET_NULL, null=True)
    description = models.TextField()
    status      = models.IntegerField(choices=STATUS_CHOICES, default=OPEN_STATUS)
    priority    = models.IntegerField(choices=PRIORITY_CHOICES, default=3)
    created     = models.DateTimeField(auto_now_add=True)
    modified    = models.DateTimeField(auto_now=True)

    objects = TicketManager()

    class Meta:

        ordering = ['-created']

    def __str__(self):
        return self.description


class FollowUp(models.Model):

    ticket  = models.ForeignKey(Ticket, related_name='followups', on_delete=models.CASCADE)
    user    = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    status  = models.IntegerField(choices=Ticket.STATUS_CHOICES, blank=True)

    def __str__(self):
        return self.comment
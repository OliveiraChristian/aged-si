from django import forms
from django.contrib.auth.models import User

from . import models


class ClientForm(forms.ModelForm):

    class Meta:

        model = models.Ticket
        fields = ['description']
        exclude = ['user', 'priority', 'ranking', ]
        labels = {
            'description': ('Descrição'),
        }


class StaffForm(forms.ModelForm):
    
    queue = forms.ChoiceField(label="Fila", choices=models.Ranking.IT_QUEUE)
    
    class Meta:

        model = models.Ticket
        
        fields = ['user', 'priority', 'queue', 'ranking', 'description']
        
        labels = {
            'description': ('Descrição'),
            'user': ('Usuário'),
            'priority': ('Prioridade'),
            'ranking': ('Requisição'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user'].queryset = User.objects.filter(is_active=True).order_by('username')       
        self.fields['ranking'].queryset = models.Ranking.objects.none()

        if 'queue' in self.data:
            try:
                queue = self.data.get('queue')
                self.fields['ranking'].queryset = models.Ranking.objects.filter(queue=queue).order_by('name')
            except (ValueError, TypeError):
                pass
        #elif self.instance.pk:#ao atualizar
        #    self.fields['ranking'].queryset = models.Ranking.objects.filter(queue=self.instance.ranking.queue).order_by('name')

    
class TicketUpdateForm(forms.ModelForm):

    queue = forms.ChoiceField(label="Fila", choices=models.Ranking.IT_QUEUE)
    
    class Meta:

        model = models.Ticket
        fields = ['priority', 'queue', 'ranking']
        labels = {
            'priority': ('Prioridade'),
            'ranking': ('Requisição'),
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['ranking'].queryset = models.Ranking.objects.none()

        if 'queue' in self.data:#ao submeter
            try:
                queue = self.data.get('queue')
                self.fields['ranking'].queryset = models.Ranking.objects.filter(queue=queue).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:#ao atualizar
            if self.instance.ranking is not None:
                self.fields['ranking'].queryset = models.Ranking.objects.filter(queue=self.instance.ranking.queue).order_by('name')
            
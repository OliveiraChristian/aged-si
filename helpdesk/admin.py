from django.contrib import admin
from django.apps import AppConfig
from .models import Ticket, FollowUp, Ranking

# Register your models here.
class RankingAdmin(admin.ModelAdmin):
    
    list_display = ['name', 'queue']
    list_filter = ('queue',)

    class Meta:
        model = Ranking

admin.site.register(Ranking, RankingAdmin)
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views import generic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import (LoginRequiredMixin,)
from django.contrib.auth.models import Group
from django.contrib import messages

from .models import Ticket, FollowUp, Ranking
from .forms import ClientForm, StaffForm, TicketUpdateForm

# Create your views here.
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


class IndexView(LoginRequiredMixin, generic.ListView):

    model = Ticket
    template_name = 'helpdesk/ticket_list.html'
    context_object_name = 'ticket_list'
    paginate_by = 2

    def get_queryset(self, *args, **kwargs):

        if has_group(self.request.user, 'helpdesk_staff'):
            return super().get_queryset().user(self.request)
        else:
            return super().get_queryset(*args, **kwargs)


class CreateTicket(LoginRequiredMixin, generic.CreateView):

    model = Ticket
    success_url = '/helpdesk/'

    def get_form(self, form_class=None):
        form = super(CreateTicket, self).get_form()
        return form

    def get_form_class(self):

        FORMS = {
            "Client": ClientForm,
            "Staff": StaffForm,
        }

        if not has_group(self.request.user, 'helpdesk_staff'):
            return FORMS['Client']
        else:
            return FORMS['Staff']

    def form_valid(self, form):
        self.object = form.save(commit=False)

        if not form.cleaned_data.get('user'):
            self.object.user = self.request.user

        self.object.save()
        return super().form_valid(form)


class TicketDetail(LoginRequiredMixin, generic.DetailView):

    model = Ticket

    def get(self, request, *args, **kwargs):
        obj = self.get_object()

        if has_group(request.user, 'helpdesk_staff') or obj.user == request.user:
            return super(TicketDetail, self).get(request, *args, **kwargs)
        else:
            messages.error(
                request, 'É possível apenas acessar os seus Tickets.')
            return redirect(reverse('tickets:index'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['follow_up_list'] = FollowUp.objects.filter(
            ticket__pk=self.kwargs.get('pk'))
        return context


class TicketUpdate(LoginRequiredMixin, generic.UpdateView):

    model = Ticket
    form_class = TicketUpdateForm
    template_name_suffix = '_update'
    success_url = '/helpdesk/'

    def get(self, request, *args, **kwargs):
        obj = self.get_object()

        if not obj.status in [Ticket.OPEN_STATUS, Ticket.REOPENED_STATUS]:
            messages.warning(request, 'O Ticket não pode ser editado.')
            return redirect(reverse('helpdesk:detail', kwargs={'pk': obj.pk}))

        if not obj.assigned_to == self.request.user:
            messages.warning(
                request, 'Apenas o técnico associado pode atualizar o Ticket.')
            return redirect(reverse('helpdesk:detail', kwargs={'pk': obj.pk}))
        else:
            return super(TicketUpdate, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, 'Ticket atualizdo com sucesso.')
        return super().form_valid(form)


class TicketDelete(LoginRequiredMixin, generic.DeleteView):

    model = Ticket
    template_name_suffix = '_confirm_delete'
    success_url = '/helpdesk/'

    #TODO fazer modal ajax da exclusao

    def delete(self, request, *args, **kwargs):

        obj = super(TicketDelete, self).get_object()

        follow_up_list = FollowUp.objects.filter(ticket__pk=obj.pk)

        if obj.assigned_to == request.user or obj.user == request.user:

            if not follow_up_list:
                messages.success(self.request, 'Ticket excluído.')
                return super(TicketDelete, self).delete(request, *args, **kwargs)
            else:
                messages.error(
                    request, 'Ticket não pode ser excluido pois possui atendimentos associados.')
        else:
            messages.warning(
                request, 'Somente o técnico associado ou o usuário pode excluir o Ticket.')

        return redirect(reverse('helpdesk:detail', kwargs={'pk': obj.pk}))


class TicketPDF(LoginRequiredMixin, generic.View):

    def get(self, request, *args, **kwargs):

        ticket = get_object_or_404(Ticket, pk=self.kwargs['pk'])

        if has_group(request.user, 'helpdesk_staff') or ticket.user == request.user:
            followup = FollowUp.objects.filter(ticket=ticket)
        else:
            messages.error(
                request, 'É possível apenas acessar os seus Tickets.')
            return redirect(reverse('tickets:index'))

        return render(request, 'helpdesk/ticket_pdf.html', {"ticket": ticket, "follow_up_list": followup, })


class SearchTicketView(LoginRequiredMixin, generic.ListView):

    model = Ticket
    context_object_name = 'ticket_list'
    template_name = 'helpdesk/ticket_list.html'

    def get_context_data(self, *args, **kwargs):
    	context = super(SearchTicketView, self).get_context_data(*args, **kwargs)
    	context['query'] = self.request.GET.get('q')
    	return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = request.GET.get('q')

        if query is not None:
            if not has_group(self.request.user, 'helpdesk_staff'):
                return Ticket.objects.search(query).filter(user=self.request.user)
            else:
                return Ticket.objects.search(query)
        return Ticket.objects.none()


def assign_to(request, pk):

    if request.method == 'GET' and has_group(request.user, 'helpdesk_staff'):

        ticket = get_object_or_404(Ticket, pk=pk)

        if ticket.assigned_to == None:
            ticket.assigned_to = request.user
            ticket.save()
            messages.success(request, 'Técnico associado com sucesso.')
        else:
            messages.warning(
                request, 'O Ticket já possui um técnico associado.')

    return redirect(reverse('helpdesk:detail', kwargs={'pk': pk}))


def add_follow_up(request, pk):

    if request.method == 'POST':

        ticket = get_object_or_404(Ticket, pk=pk)

        if ticket.assigned_to == request.user:
            if not ticket.ranking:
                messages.warning(request, 'O ticket precisa ser classificado primeiro.')
            else:
                follow_up = FollowUp(
                    ticket=ticket,
                    user=request.user,
                    comment=request.POST['comment'],
                    status=request.POST['status']
                )
                follow_up.save()

                ticket.status = request.POST['status']
                ticket.save()

                messages.success(request, 'Atendimento cadastrado com sucesso.')
        else:
            messages.warning(
                request, 'Apenas o técnico associado pode adicionar um atendimento.')

    return redirect(reverse('helpdesk:detail', kwargs={'pk': pk}))


def load_rankings(request):
    queue_id = request.GET.get('queue')
    rankings = Ranking.objects.filter(queue=queue_id).order_by('name')
    return render(request, 'helpdesk/ranking_dropdown_list_options.html', {'rankings': rankings})

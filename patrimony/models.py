from django.db import models
from django.contrib.auth import get_user_model
from organization.models import Unit
from django.db.models import Q

# Create your models here.
User = get_user_model()

class State(models.Model):
    CONSERVATION_STATE = (
            (0, 'Novo'),
            (1, 'Bom'),
            (2, 'Ruim'),
            (3, 'Inservível')
        )

#########################
#Tipo de Bem Patrimonial
#########################
class EquipmentType(models.Model):
    equipment_type = models.CharField(verbose_name='tipo de bem patrimonial', max_length=100, unique=True)

    def clean(self):
        self.equipment_type = self.equipment_type.capitalize()

    def __str__(self):
        return self.equipment_type
    
    class Meta:
        permissions = (("can_access_patrimony", "pode acessar patrimonio"),
            ("can_authorize_moviment", "pode autorizar movimentacoes"),)

##########################
#Bem Patrimonial
##########################
######QuerySet######
class EquipmentQuerySet(models.query.QuerySet):
    def search(self, query):
        lookups = (
            Q(location__name__icontains=query)
        )
        return self.filter(lookups).distinct()

######Manager######
class EquipmentManager(models.Manager):
    def get_queryset(self):
        return EquipmentQuerySet(self.model, using=self._db)

    def search(self, query):
        return self.get_queryset().search(query)
########Equipment##########
class Equipment(models.Model):

    equipment_type = models.ForeignKey(
        EquipmentType, related_name='equipamento', on_delete=models.SET_NULL, null=True)
    name = models.CharField('bem patrimonial', max_length=150)
    tombo = models.PositiveSmallIntegerField(null=True, blank=True, default=True)
    location = models.ForeignKey(Unit, on_delete=models.SET_NULL, null=True)
    conservation = models.IntegerField('estado de conservação', choices=State.CONSERVATION_STATE, null=False)
    description = models.TextField('descrição')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    active = models.BooleanField('ativo', default=True)

    objects = EquipmentManager()

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name

###################################
#Movimentação de Patrimonio
###################################
######QuerySet######
class MovimentRequestQuerySet(models.query.QuerySet):
    def user(self, request):
        return self.filter(user=request.user)
    
    def search(self, query):
        lookups = (
            Q(pk__icontains=query) |
            Q(user__username__icontains=query) |
            Q(destiny__name__icontains=query)
        )
        return self.filter(lookups).distinct()
    
    def units(self, query):
        lookups = (
            Q(unit__pk__in=query)
        )
        return self.filter(lookups).distinct()

######Manager######
class MovimentRequestManager(models.Manager):
    def get_queryset(self):
        return MovimentRequestQuerySet(self.model, using=self._db)

    def search(self, query):
        return self.get_queryset().search(query)

class MovimentRequest(models.Model):
    OPEN_STATUS = 1
    AUTHORIZED_STATUS = 2
    CLOSED_STATUS = 3

    STATUS_CHOICES = (
        (OPEN_STATUS, 'Aberto / Não Autorizado'),
        (AUTHORIZED_STATUS, 'Autorizado'),
        (CLOSED_STATUS, 'Transferido'),
    )
    user = models.ForeignKey(
        User, related_name='moviment_request', on_delete=models.SET_NULL, null=True)
    destiny = models.ForeignKey(Unit, on_delete=models.SET_NULL, null=True)
    authorized_by = models.ForeignKey(
        User, related_name='authorized_request', on_delete=models.SET_NULL, null=True)
    authorized_date = models.DateTimeField(blank=True, null=True)
    created = models .DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=OPEN_STATUS)
    obs = models.TextField(blank=True, null=True)

    objects = MovimentRequestManager()

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.destiny

# ##########################################
# #Tombos na movimentações
# ##########################################
# #################
# #QuerySet
# #################
class TombosAddQuerySet(models.query.QuerySet):

    def search(self, query):
        lookups = (
            Q(tombo__icontains=query)
        )
        return self.filter(lookups).distinct()

# ################
# #Manager
# ################
class TombosAddManager(models.Manager):

    def get_queryset(self):
        return TombosAddQuerySet(self.model, using=self._db)

    def search(self, query):
        return self.get_queryset().search(query)

class TombosAddMoviment(models.Model):

    moviment = models.ForeignKey(MovimentRequest, on_delete=models.SET_NULL, null=True)
    tombo = models.ForeignKey(Equipment, on_delete=models.SET_NULL, null=True)
    new_location = models.CharField(max_length=250, null=True, blank=True)
    previous_location = models.CharField(max_length=250, null=True, blank=True)
    date_moviment = models.DateTimeField(null=True)
    previous_conservation = models.IntegerField(choices=State.CONSERVATION_STATE, null=True)
    new_conservation = models.IntegerField(choices=State.CONSERVATION_STATE, null=False)


    objects = TombosAddManager()

    def __int__(self):
        return self.tombo
// Modal
$(function () {

  /* Functions */
  //Carregar o modal
  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal .modal-content").html("");
        $("#modal").modal("show");
      },
      success: function (data) {
        $("#modal .modal-content").html(data.html_form);
      }
    });
  };
  
  //Atualizar uma movimentação
  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload();
        }
        else {
          $("#modal .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };
  //Deletar uma movimentação
  var deleteForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.href = "/patrimony/moviment/";
        }
        else {
          window.location.reload()
        }
      }
    });
    return false;
  };

  var searchTombo = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#tombo-table-disponible tbody").html(data.html_tombo_list);
        }
        else {
          window.location.reload()
        }
      }
    });
    return false;
  };

  /* Binding */
// update moviment
$(".js-update-moviment").click(loadForm);
$("#modal").on("submit", ".js-moviment-update-form", saveForm);

// delete moviment
$(".js-delete-moviment").click(loadForm);
$("#modal").on("submit", ".js-moviment-delete-form", deleteForm);

// pesquisar tombos
$(".js-search-tombo-moviment").submit(searchTombo);

});
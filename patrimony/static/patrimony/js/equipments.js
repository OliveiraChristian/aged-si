$(function () {

    /* Functions */
    //Carregar o modal
    var loadForm = function () {
      var btn = $(this);
      $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
          $("#modal .modal-content").html("");
          $("#modal").modal("show");
        },
        success: function (data) {
          $("#modal .modal-content").html(data.html_form);
        }
      });
    };
    //Criar e Atualizar um equipamento
    var saveForm = function () {
      var form = $(this);
      $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: form.attr("method"),
        dataType: 'json',
        success: function (data) {
          if (data.form_is_valid) {
            window.location.reload();
          }
          else {
            $("#modal .modal-content").html(data.html_form);
          }
        }
      });
      return false;
    };
    //Deletar um equipamento
    var deleteForm = function () {
      var form = $(this);
      $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: form.attr("method"),
        dataType: 'json',
        success: function (data) {
          if (data.form_is_valid) {
            window.location.href = "/patrimony/equipment/";
          }
        }
      });
      return false;
    };
  
    /* Binding */
  
    // Create equipment
    $(".js-create-equipment").click(loadForm);
    $("#modal").on("submit", ".js-equipment-create-form", saveForm);

    // update equipment
    $(".js-update-equipment").click(loadForm);
    $("#modal").on("submit", ".js-equipment-update-form", saveForm);

    // delete equipment
    $(".js-delete-equipment").click(loadForm);
    $("#modal").on("submit", ".js-equipment-delete-form", deleteForm);
  
  });
# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-07-18 12:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('patrimony', '0034_auto_20180718_0859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tombosaddmoviment',
            name='moviment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tomboos_add', to='patrimony.PatrimonyRequest'),
        ),
    ]

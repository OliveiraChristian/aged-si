# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-09-11 13:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0011_auto_20180222_1606'),
        ('patrimony', '0067_auto_20180903_1133'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movimentrequest',
            name='authorized_by',
        ),
        migrations.RemoveField(
            model_name='movimentrequest',
            name='destiny',
        ),
        migrations.RemoveField(
            model_name='movimentrequest',
            name='user',
        ),
        migrations.RemoveField(
            model_name='registertombo',
            name='equipment',
        ),
        migrations.RemoveField(
            model_name='registertombo',
            name='location',
        ),
        migrations.RemoveField(
            model_name='tombosaddmoviment',
            name='moviment',
        ),
        migrations.RemoveField(
            model_name='tombosaddmoviment',
            name='tombos',
        ),
        migrations.AddField(
            model_name='equipment',
            name='conservation',
            field=models.IntegerField(choices=[(0, 'Novo'), (1, 'Bom'), (2, 'Ruim'), (3, 'Inservível')], default=True),
        ),
        migrations.AddField(
            model_name='equipment',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='organization.Unit'),
        ),
        migrations.AddField(
            model_name='equipment',
            name='tombo',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.DeleteModel(
            name='MovimentRequest',
        ),
        migrations.DeleteModel(
            name='RegisterTombo',
        ),
        migrations.DeleteModel(
            name='TombosAddMoviment',
        ),
    ]

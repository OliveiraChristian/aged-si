# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-08-20 13:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('patrimony', '0054_auto_20180806_1025'),
    ]

    operations = [
        migrations.CreateModel(
            name='Eac',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('eac', models.CharField(max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name='Ulsav',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ulsav', models.CharField(max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name='Ur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ur', models.CharField(max_length=150, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='ulsav',
            name='ur',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='patrimony.Ur'),
        ),
        migrations.AddField(
            model_name='eac',
            name='ulsav',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='patrimony.Ulsav'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-07-11 13:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patrimony', '0022_auto_20180711_0856'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='movimenttombos',
            options={'ordering': ['moviment']},
        ),
    ]

from django.test import TestCase
from django.shortcuts import resolve_url as r

# Create your tests here.

class HomeTest(TestCase):
    def setUp(self):
        self.response = self.client.get(r('home'))

    def test_get(self):
        self.assertEqual(200, self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response, 'home_patrimony.html')

    # def setUp(self):
    #     self.url = reverse('index')

    # def test_get(self):
    #     response = self.client.get(self.url)
    #     self.assertEqual(200, response.status_code)

    # def test_template(self):
    #     self.assertTemplateUsed(self.response, 'home_patrimony.html')
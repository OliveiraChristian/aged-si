from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.views import generic
import random

from .models import EquipmentType, Equipment, MovimentRequest, TombosAddMoviment
from .forms import TypeEquipmentForm, EquipmentForm, MovimentRequestForm, SearchTombosForm, EquipmentEditForm

#########################
#Home
#########################
@login_required
def homePatrimony(request):
    if request.user.has_perm('patrimony.can_access_patrimony'):
        return render(request, 'patrimony/home_patrimony.html')
    else:
        messages.warning(request, 'Você não possui permissão para acessar essa funcionalidade.')
        return HttpResponseRedirect('/')

##########################################
#Classes e Funções para bens patrimoniais
##########################################
class EquipmentList(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    permission_required = 'patrimony.can_access_patrimony'
    model = Equipment
    template_name = 'patrimony/equipment/equipment_list.html'
    context_object_name = 'equipments'
    paginate_by = 10

    def get_context_data(self, **kwargs):
    	context = super().get_context_data(**kwargs)
    	context['search_tombo'] = SearchTombosForm()
    	return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        form = SearchTombosForm(request.GET)
        query = form.save(commit=False)

        if (query.equipment_type and query.location) is not None:
            equipments = Equipment.objects.search(query.location).filter(model=query.equipment_type)
        elif (query.equipment_type is not None) and (query.location is None):
            equipments = Equipment.objects.all().filter(model=query.equipment_type)
        elif (query.equipment_type is None) and (query.location is not None):
            equipments = Equipment.objects.all().filter(location=query.location)
        else:
            equipments = Equipment.objects.all()
        return equipments

@login_required
@permission_required('patrimony.can_access_patrimony')
def equipment_create(request):
    data = dict()
    if request.method == 'POST':
        form = EquipmentForm(request.POST)
        if form.is_valid():
            data['form_is_valid'] = True
            equipment = form.save(commit=False)
            if equipment.tombo is not None:
                tombo_existe = Equipment.objects.filter(tombo=equipment.tombo)
                if tombo_existe.exists():
                    messages.error(request, 'Bem patrimonial já existe.')
                else:
                    form.save()
                    equipments = Equipment.objects.all()
                    data['html_equipment_list'] = render_to_string('patrimony/equipment/equipment_list.html',
                        { 'equipments': equipments })
                    messages.success(request, 'Bem patrimonial cadastrado com sucesso.')
            else:
                form.save()
                equipments = Equipment.objects.all()
                data['html_equipment_list'] = render_to_string('patrimony/equipment/equipment_list.html',
                    { 'equipments': equipments })
                messages.success(request, 'Bem patrimonial cadastrado com sucesso.')
        else:
            data['form_is_valid'] = False
    else:
        form = EquipmentForm()
        data['form_is_valid'] = False
        context = {'form': form}
        data['html_form'] = render_to_string('patrimony/equipment/equipment_create.html',
            context, request=request)
    return JsonResponse(data)

@login_required
@permission_required('patrimony.can_access_patrimony')
def equipment_update(request, pk):
    equipment = get_object_or_404(Equipment, pk=pk)
    data = dict()
    if request.method == 'POST':
        form = EquipmentEditForm(request.POST, instance=equipment)
        if form.is_valid():
            data['form_is_valid'] = True
            form.save()
            messages.success(request, 'Bem patrimonial atualizado com sucesso.')
        else:
            data['form_is_valid'] = False
    else:
        form = EquipmentEditForm(instance=equipment)
        data['form_is_valid'] = False
        context = {'form':form}
        data['html_form'] = render_to_string('patrimony/equipment/equipment_update.html', context, request=request)
    return JsonResponse(data)

@login_required
@permission_required('patrimony.can_access_patrimony')
def equipment_delete(request, pk):
    equipment = get_object_or_404(Equipment, pk=pk)
    data = dict()
    if request.method == 'POST':
        equipment.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Bem patrimonial excluído com sucesso.')
    else:
        context = {'equipment': equipment}
        data['html_form'] = render_to_string('patrimony/equipment/equipment_delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)

class DetailEquipmentView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    permission_required = 'patrimony.can_access_patrimony'
    model = Equipment
    success_url = '/patrimony/equipment'
    template_name = 'patrimony/equipment/equipment_detail.html'

@login_required
@permission_required('patrimony.can_access_patrimony')
def equipment_responsibility(request, pk):
    equipment = get_object_or_404(Equipment, pk=pk)
    date = timezone.now()
    context = { 'equipment': equipment, 'date': date }
    return render(request, 'patrimony/equipment/responsibility.html', context)

@login_required
@permission_required('patrimony.can_access_patrimony')
def tombo_history(request, pk):
    equipment = get_object_or_404(Equipment,  pk=pk)
    moviments = TombosAddMoviment.objects.filter(tombo__pk=equipment.pk).order_by('date_moviment')
    context = {'equipment':equipment, 'moviments':moviments}
    return render(request, 'patrimony/tombo/tombo_history_pdf.html', context)

##############################
#Movimentação de Patrimônio
##############################
class MovimentList(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    permission_required = 'patrimony.can_access_patrimony'
    model = MovimentRequest
    template_name = 'patrimony/moviment/moviment_list.html'
    context_object_name = 'moviments'
    paginate_by = 10

@login_required
@permission_required('patrimony.can_access_patrimony')
def moviment_create(request):
    if request.method == 'POST':
        form = MovimentRequestForm(request.POST)
        if form.is_valid():
            moviment = form.save(commit=False)
            moviment.user = request.user
            moviment.save()
            return HttpResponseRedirect(reverse('patrimony:moviment_detail', kwargs={'pk': moviment.pk}))
    else:
        form = MovimentRequestForm()
    return render(request, 'patrimony/moviment/moviment_create.html', {'form': form})

@login_required
@permission_required('patrimony.can_access_patrimony')
def moviment_update(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    data = dict()
    if request.method == 'POST':
        form = MovimentRequestForm(request.POST, instance=moviment)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
    else:
        form = MovimentRequestForm(instance=moviment)
        data['form_is_valid'] = False
        context = {'form': form}
        data['html_form'] = render_to_string('patrimony/moviment/moviment_update.html',
            context, request=request)
    return JsonResponse(data)

@login_required
@permission_required('patrimony.can_access_patrimony')
def moviment_delete(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    data = dict()
    if request.method == 'POST':
        if moviment.status != 3:
            moviment.delete()
            data['form_is_valid'] = True
            messages.success(request, 'Movimentação excluída com sucesso.')
        else:
            messages.warning(request, 'Movimentação já finalizada.')
    else:
        context = {'moviment': moviment}
        data['html_form'] = render_to_string('patrimony/moviment/moviment_delete.html',
            context, request=request,)
    return JsonResponse(data)

@login_required
@permission_required('patrimony.can_access_patrimony')
def moviment_detail(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    tombos_add = TombosAddMoviment.objects.filter(moviment__pk=pk)
    form = SearchTombosForm()
    context = {'moviment':moviment, 'form':form, 'tombos_add':tombos_add}
    return render(request, 'patrimony/moviment/moviment_detail.html', context)

class SearchMoviment(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    permission_required = 'patrimony.can_access_patrimony'
    model = MovimentRequest
    context_object_name = 'moviments'
    template_name = 'patrimony/moviment/moviment_list.html'

    def get_context_data(self, *args, **kwargs):
    	context = super(SearchMoviment, self).get_context_data(*args, **kwargs)
    	context['query'] = self.request.GET.get('q')
    	return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = request.GET.get('q')
        if query is not None:
            return MovimentRequest.objects.search(query)

#################################
#Tombos para movimentação
#################################
@login_required
@permission_required('patrimony.can_access_patrimony')
def search_tombo_moviment(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    data = dict()
    if request.method == 'GET':
        form = SearchTombosForm(request.GET)
        query = form.save(commit=False)
        if (query.equipment_type and query.location) is not None:
            equipments = Equipment.objects.search(query.location).filter(model=query.equipment_type)
        elif (query.equipment_type is not None) and (query.location is None):
            equipments = Equipment.objects.all().filter(model=query.equipment_type)
        elif (query.equipment_type is None) and (query.location is not None):
            equipments = Equipment.objects.all().filter(location=query.location)
        else:
            equipments = Equipment.objects.all()

        if equipments.exists():
            data['form_is_valid'] = True
            data['html_tombo_list'] = render_to_string('patrimony/tombo/search_tombo_moviment.html',
                { 'equipments': equipments, 'moviment':moviment })
        else:
            data['form_is_valid'] = False
    return JsonResponse(data)

@login_required
@permission_required('patrimony.can_access_patrimony')
def add_tombo_moviment(request, pk_m, pk_t):
    moviment = get_object_or_404(MovimentRequest, pk=pk_m)
    tombo = get_object_or_404(Equipment, pk=pk_t)
    conservation = request.GET.get('select')
    validate = TombosAddMoviment.objects.filter(moviment__pk=moviment.pk, tombo__pk=tombo.pk)
    if not validate.exists():#verifica se o tombo ja está adicionado na movimentação
        t = TombosAddMoviment.objects.create(moviment=moviment, tombo=tombo, new_conservation=conservation)
        #Retorna para o status de Aberto/Não autorizado caso o usuario add mais tombos
        if moviment.status == 2:
            moviment.status = MovimentRequest.OPEN_STATUS
        moviment.save()
        messages.success(request, 'Tombo %s adicionado com sucesso.' %(t.tombo.tombo))
    else:
        messages.error(request, 'Tombo já adicionado na movimentação.')
    return HttpResponseRedirect(reverse('patrimony:moviment_detail', kwargs={'pk': moviment.pk}))

@login_required
@permission_required('patrimony.can_access_patrimony')
def del_tombo_moviment(request, pk_m, pk_t):
    moviment = get_object_or_404(MovimentRequest, pk=pk_m)
    tombo = get_object_or_404(Equipment, pk=pk_t)
    t = TombosAddMoviment.objects.filter(moviment__pk=moviment.pk, tombo__pk=tombo.pk)
    #Retorna para o status de Aberto/Não autorizado caso o usuario exclua tombos da movimentação
    if moviment.status != 3:#verifica se a movimentação já foi finalizada
        if moviment.status == 2:
            moviment.status = MovimentRequest.OPEN_STATUS
            moviment.save()
        t.delete()
        messages.success(request, 'Tombo excluído com sucesso.')
    return HttpResponseRedirect(reverse('patrimony:moviment_detail', kwargs={'pk': moviment.pk}))

##################################
#Status da Movimentação
##################################
@login_required
@permission_required('patrimony.can_authorize_moviment')
def authorize(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    tombos_add = TombosAddMoviment.objects.filter(moviment__pk=moviment.pk)
    qtd_tombos = len(tombos_add)
    #verifica se há tombos na movimentação para poder autorizar
    if qtd_tombos != 0:
        moviment.authorized_by = request.user
        moviment.authorized_date = timezone.now()
        moviment.status = MovimentRequest.AUTHORIZED_STATUS
        moviment.save()
        messages.success(request, 'Solicitação autorizada com sucesso.')
    else:
        messages.error(request, 'Não há tombos nessa movimentação.')
    return redirect(reverse('patrimony:moviment_detail', kwargs={'pk': pk}))

@login_required
@permission_required('patrimony.can_authorize_moviment')
def finish(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    tombos_add = TombosAddMoviment.objects.filter(moviment__pk=moviment.pk)
    if moviment.status != 1:#Antes de finalizar verifica se a movimentação não foi alterada por outro usuario simultaneamente
        for tombo in tombos_add:
            tombo_request = get_object_or_404(Equipment, pk=tombo.tombo_id)
            tombo_moviment = get_object_or_404(TombosAddMoviment, pk=tombo.id)
            #pega o local anterior e o novo local do tombo e registra no BD
            tombo_moviment.previous_location = tombo_request.location.name
            tombo_moviment.new_location = moviment.destiny.name
            tombo_moviment.date_moviment = timezone.now()
            tombo_moviment.previous_conservation = tombo_request.conservation
            tombo_moviment.save()
                
            #altera a nova localização do tombo no BD
            tombo_request.location = moviment.destiny
            tombo_request.conservation = tombo_moviment.new_conservation
            tombo_request.save()

        moviment.status = MovimentRequest.CLOSED_STATUS
        moviment.save()
        messages.success(request, 'Solicitação finalizada com sucesso.')
    else:
        messages.warning(request, 'Solicitação alterada.')
    return redirect(reverse('patrimony:moviment_detail', kwargs={'pk': pk}))

@login_required
@permission_required('patrimony.can_access_patrimony')
def responsibility(request, pk):
    moviment = get_object_or_404(MovimentRequest, pk=pk)
    tombos = TombosAddMoviment.objects.filter(moviment__pk=moviment.pk).order_by('previous_location')
    date = timezone.now()
    context = { 'moviment': moviment, 'tombos': tombos, 'date': date }
    return render(request, 'patrimony/moviment/responsibility.html', context)
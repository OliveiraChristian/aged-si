from django.conf.urls import url

from . import views
from .views import homePatrimony

urlpatterns = [
    url(r'^$', homePatrimony, name='index'),#pagina inicial de patrimônio

#urls de equipamentos
    url(r'^equipment/$', views.EquipmentList.as_view(), name='equipment'),#lista de equipamentos
    url(r'^equipment/create$', views.equipment_create, name='register_equipment'),#cadastrar equipamento
    url(r'^equipment/(?P<pk>\d+)/detail/$', views.DetailEquipmentView.as_view(), name='detail_equipment'),#detalhar equipamento
    url(r'^equipment/(?P<pk>\d+)/delete/$', views.equipment_delete, name='equipment_delete'),#deletar equipamento
    url(r'^equipment/(?P<pk>\d+)/update/$', views.equipment_update, name='equipment_update'),#atualizar equipamento
    url(r'^equipment/(?P<pk>\d+)/responsibility$', views.equipment_responsibility, name='equipment_responsibility'),#Termo de Responsabilidade para novos objetos
    url(r'^equipment/(?P<pk>\d+)/detail/tombo_history$', views.tombo_history, name='tombo_history'),#histórico de tombo

#urls de movimentações
    url(r'^moviment/$', views.MovimentList.as_view(), name='moviment'),#lista de movimentações
    url(r'^moviment/create$', views.moviment_create, name='moviment_create'),#requisitar movimentações
    url(r'^moviment/search$', views.SearchMoviment.as_view(), name='moviment_search'),#Pesquisar por movimentações
    url(r'^moviment/(?P<pk>\d+)/detail/$', views.moviment_detail, name='moviment_detail'),#detalhar movimentação
    url(r'^moviment/(?P<pk>\d+)/update/$', views.moviment_update, name='moviment_update'),#atualizar movimentação
    url(r'^moviment/(?P<pk>\d+)/delete/$', views.moviment_delete, name='moviment_delete'),#deletar movimentação

#urls tombos na movimentação
    url(r'^moviment/create/search_tombo(?P<pk>\d+)$', views.search_tombo_moviment, name='search_tombo'),#Pesquisar tombos para movimentação
    url(r'^moviment/(?P<pk_m>\d+)/detail/add_tombo(?P<pk_t>\d+)$', views.add_tombo_moviment, name='add_tombo'),#adicionar tombos a movimentação
    url(r'^moviment/(?P<pk_m>\d+)/detail/del_tombo(?P<pk_t>\d+)$', views.del_tombo_moviment, name='del_tombo'),#adicionar tombos a movimentação

#urls autorizar movimentações
    url(r'^moviment/(?P<pk>\d+)/authorize$', views.authorize, name='authorize'),#Autorizar movimentação
    url(r'^moviment/(?P<pk>\d+)/finish$', views.finish, name='finish'),#Finalizar movimentação
    url(r'^moviment/(?P<pk>\d+)/responsibility$', views.responsibility, name='responsibility'),#Termo de Responsabilidade da movimentação
]
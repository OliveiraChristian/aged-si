from django.contrib import admin
from .models import EquipmentType, Equipment, MovimentRequest

admin.site.register(EquipmentType)

class EquipmentAdmin(admin.ModelAdmin):
    list_display=('equipment_type', 'name', 'tombo', 'location',)
    list_filter=('equipment_type',)
    search_fields=('location__name',)

admin.site.register(Equipment, EquipmentAdmin)
from django import forms
from django.contrib.auth.models import User

from . import models

#######Form para tipo de equipamento############
class TypeEquipmentForm(forms.ModelForm):
    class Meta:
        model = models.EquipmentType
        fields = ['equipment_type']
        labels = {
            'equipment_type': ('Tipo de Bem Patrimonial'),
        }

######Form para equipamentos#################
class EquipmentForm(forms.ModelForm):
    class Meta:
        model = models.Equipment
        fields = ['equipment_type', 'name', 'tombo', 'location', 'conservation', 'description']
        labels = {
            'equipment_type':'Tipo de Bem Patrimonial',
            'name':'Marca do Bem Patrimonial',
            'tombo':'Tombo',
            'location':'Localização',
            'conservation':'Estado de Conservação',
            'description':'Descrição',
        }

        widgets = {'description': forms.Textarea(attrs={'rows': '3'})}

class EquipmentEditForm(forms.ModelForm):
    class Meta:
        model = models.Equipment
        fields = ['equipment_type', 'name', 'description']
        labels = {
            'equipment_type':'Tipo de Bem Patrimonial',
            'name':'Marca do Bem Patrimonial',
            'description':'Descrição',
        }

        widgets = {'description': forms.Textarea(attrs={'rows':'3'})}

#######Form para movimentação de patrimônio#########
class MovimentRequestForm(forms.ModelForm):
    class Meta:
        model = models.MovimentRequest
        fields = ['destiny', 'obs']
        
        labels = {
            'destiny': ('Destino'),
            'obs': ('Observação'),
        }

        widgets = { 'obs': forms.Textarea(attrs={'rows': '3'}), }

class SearchTombosForm(forms.ModelForm):
    # location = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Localização'}))
    class Meta:
        model = models.Equipment
        fields = ['equipment_type', 'location']
        labels = {'equipment_type':'Tipo', 'location':'Localização'}
        # widgets = {'model': forms.TextInput(attrs={'mr': '3'}),}
    def __init__(self, *args, **kwargs):
        super(SearchTombosForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].required = False

class AddTombosForm(forms.ModelForm):
    class Meta:
        model = models.TombosAddMoviment
        fields = ['moviment', 'tombo']
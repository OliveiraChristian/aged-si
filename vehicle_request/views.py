from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views import generic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import (LoginRequiredMixin,)
from django.contrib.auth.models import Group
from django.contrib import messages
from django.contrib.auth.models import User
from django.utils import timezone


from organization.models import Unit, UnitUser

from .models import VehicleRequest, Vehicle, VehicleModel
from .forms import VehicleRequestForm

# Create your views here.


def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


class IndexView(LoginRequiredMixin, generic.ListView):

    model = VehicleRequest
    template_name = 'vehicle_request/vehicle_request_list.html'
    context_object_name = 'vehicle_request_list'
    paginate_by = 15

    def get_queryset(self, *args, **kwargs):

        if has_group(self.request.user, 'transport_staff'):  # staff transporte
            return super().get_queryset(*args, **kwargs)
        elif has_group(self.request.user, 'transport_accountable'):  # pode autorizar
            return super().get_queryset().units(UnitUser.objects.filter(user=self.request.user).values('unit'))
        else:
            return super().get_queryset().user(self.request)


class CreateVehicleRequest(LoginRequiredMixin, generic.CreateView):

    model = VehicleRequest
    success_url = '/vehicle_request/'
    template_name = 'vehicle_request/vehicle_request_form.html'
    form_class = VehicleRequestForm

    def get_form_kwargs(self):
        kwargs = super(CreateVehicleRequest, self).get_form_kwargs()
        kwargs['initial'] = {'user': self.request.user}
        return kwargs

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        return super().form_valid(form)


class VehicleRequestDetail(LoginRequiredMixin, generic.DetailView):

    model = VehicleRequest
    template_name = 'vehicle_request/vehicle_request_detail.html'
    context_object_name = 'vehicle_request'

    def get(self, request, *args, **kwargs):
        obj = self.get_object()

        if has_group(self.request.user, 'transport_staff'):
            return super(VehicleRequestDetail, self).get(request, *args, **kwargs)

        user_units = Unit.objects.filter(
            pk__in=UnitUser.objects.filter(user=request.user).values('unit'))

        #Permitir visualizar apenas se for do setor
        if obj.unit in user_units:
            return super(VehicleRequestDetail, self).get(request, *args, **kwargs)
        else:
            messages.error(
                request, 'É possível apenas acessar as suas Solicitações.')
            return redirect(reverse('vehicle_request:index'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        #Se tiver autorizado passa uma lista de veiculos e de usuarios
        if context['vehicle_request'].status == VehicleRequest.AUTHORIZED_STATUS:
            context['user_list'] = User.objects.all().order_by('username')
            context['vehicle_list'] = Vehicle.objects.all(
            ).available().order_by('license_plate')
        return context


class VehicleRequestDelete(LoginRequiredMixin, generic.DeleteView):

    model = VehicleRequest
    template_name_suffix = '_confirm_delete'
    success_url = '/vehicle_request/'

    #TODO fazer modal ajax da exclusao

    def delete(self, request, *args, **kwargs):

        obj = super(VehicleRequestDelete, self).get_object()

        if obj.user == request.user or has_group(request.user, 'transport_staff'):

            if not str(obj.status) in '1,2'.split(','):
                messages.error(request, 'Solicitação não pode ser excluida.')
            else:
                messages.success(self.request, 'Solicitação excluída.')
                return super(VehicleRequestDelete, self).delete(request, *args, **kwargs)
        else:
            messages.warning(
                request, 'Somente o técnico associado ou o usuário pode excluir a Solicitação.')

        return redirect(reverse('vehicle_request:index'))


class VehicleRequestPDF(LoginRequiredMixin, generic.View):

    def get(self, request, *args, **kwargs):

        vehicle_request = get_object_or_404(
            VehicleRequest, pk=self.kwargs['pk'])

        if has_group(request.user, 'transport_staff') or vehicle_request.user == request.user:
            return render(request, 'vehicle_request/vehicle_request_pdf.html', {"vehicle_request": vehicle_request, })
        messages.error(
            request, 'É possível apenas acessar as suas Solicitações.')
        return redirect(reverse('vehicle_request:index'))


class SearchVehicleRequestView(LoginRequiredMixin, generic.ListView):

    model = VehicleRequest
    context_object_name = 'vehicle_request_list'
    template_name = 'vehicle_request/vehicle_request_list.html'

    def get_context_data(self, *args, **kwargs):
    	context = super(SearchVehicleRequestView,
    	                self).get_context_data(*args, **kwargs)
    	context['query'] = self.request.GET.get('q')
    	return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = request.GET.get('q')

        if query is not None:
            #Se for do setor de transporte retorna tudo
            if has_group(self.request.user, 'transport_staff'):
                return VehicleRequest.objects.search(query)
            #Se for responsavel retorna dos setores que pertence
            elif has_group(self.request.user, 'transport_accountable'):
                return VehicleRequest.objects.search(query).filter(
                    unit__pk__in=UnitUser.objects.filter(user=self.request.user).values('unit')
                    )
            #usuario comum visualiza apenas as suas
            else:
                return VehicleRequest.objects.search(query).filter(user=self.request.user)

        return VehicleRequest.objects.none()


def authorize(request, pk):

    if request.method == 'GET' and has_group(request.user, 'transport_accountable'):

        vehicle_request = get_object_or_404(VehicleRequest, pk=pk)

        if vehicle_request.status == 1:

            user_units = Unit.objects.filter(
                pk__in=UnitUser.objects.filter(user=request.user).values('unit'))

            #se for do setor
            if vehicle_request.unit in user_units:

                vehicle_request.authorized_by = request.user
                vehicle_request.authorized_date = timezone.now()
                vehicle_request.status = VehicleRequest.AUTHORIZED_STATUS
                vehicle_request.save()

                messages.success(
                    request, 'Solicitação autorizada com sucesso.')
            else:
                messages.warning(
                    request, 'Você não tem permissão para autorizar a solicitação.')

    else:
        messages.warning(
            request, 'Você não tem permissão para autorizar a solicitação.')

    return redirect(reverse('vehicle_request:detail', kwargs={'pk': pk}))


def attend(request, pk):

    if request.method == 'POST' and has_group(request.user, 'transport_staff'):

        vehicle_request = get_object_or_404(VehicleRequest, pk=pk)

        if vehicle_request.status == 2:

            driver = User.objects.get(pk=request.POST.get('user'))
            vehicle = Vehicle.objects.get(pk=request.POST.get('vehicle'))

            vehicle_request.driver = driver
            vehicle_request.vehicle = vehicle
            vehicle_request.departure_date = timezone.now()
            vehicle_request.status = VehicleRequest.IN_ATTENDANCE_STATUS
            vehicle_request.save()

            vehicle.status = False
            vehicle.save()

            messages.success(request, 'Solicitação em atendimento.')

    else:
        messages.warning(
            request, 'Apenas o usuário do Transporte pode atender a solicitação.')

    return redirect(reverse('vehicle_request:detail', kwargs={'pk': pk}))


def finish(request, pk):

    if request.method == 'POST' and has_group(request.user, 'transport_staff'):

        vehicle_request = get_object_or_404(VehicleRequest, pk=pk)

        if vehicle_request.status == 3:

            vehicle_request.status = VehicleRequest.CLOSED_STATUS
            vehicle_request.arrival_date = timezone.now()
            vehicle_request.obs = request.POST.get('obs')
            vehicle_request.save()

            vehicle = Vehicle.objects.get(pk=vehicle_request.vehicle.pk)
            vehicle.status = True
            vehicle.save()

            messages.success(request, 'Solicitação finalizada.')

    else:
        messages.warning(
            request, 'Apenas o usuário do Transporte pode finalizar a solicitação.')

    return redirect(reverse('vehicle_request:detail', kwargs={'pk': pk}))

from django.db import models
from django.contrib.auth import get_user_model
from organization.models import Unit
from django.db.models import Q

# Create your models here.
User = get_user_model()


class VehicleModel(models.Model):

    BRAND_CHOICES = (
        (1, 'Toyota'),
        (2, 'Volkswagen'),
        (3, 'Ford'),
        (4, 'Chevrolet'),
        (5, 'Hyundai'),
        (6, 'Nissan'),
        (7, 'Honda'),
        (8, 'Kia'),
        (9, 'Fiat'),
        (10, 'Renault'),
        (11, 'Peugeot'),
        (12, 'Suzuki'),
        (13, 'Citroën'),
        (14, 'Mitsubishi'),
    )

    brand = models.IntegerField(choices=BRAND_CHOICES)
    model = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.model


class VehicleQuerySet(models.query.QuerySet):

    def available(self):
        return self.filter(status=True)


class VehicleManager(models.Manager):

    def get_queryset(self):
        return VehicleQuerySet(self.model, using=self._db)


class Vehicle(models.Model):

    model = models.ForeignKey(
    VehicleModel, related_name='vehicles', on_delete=models.SET_NULL, null=True)
    license_plate = models.CharField(max_length=7, unique=True)
    active = models.BooleanField(default=True)
    status = models.BooleanField(default=True)

    objects = VehicleManager()

    def __str__(self):
        return self.license_plate


class VehicleRequestQuerySet(models.query.QuerySet):

    def user(self, request):
        return self.filter(user=request.user)

    def search(self, query):
        lookups = (
            Q(user__username__icontains=query) |
            Q(pk__icontains=query)
        )
        return self.filter(lookups).distinct()

    def units(self, query):
        lookups = (
            Q(unit__pk__in=query)
        )
        return self.filter(lookups).distinct()


class VehicleRequestManager(models.Manager):

    def get_queryset(self):
        return VehicleRequestQuerySet(self.model, using=self._db)

    def search(self, query):
        return self.get_queryset().search(query)


class VehicleRequest(models.Model):

    REQUEST_CHOICES = (
        (1, 'Interno'),
        (2, 'Viagem'),
    )

    OPEN_STATUS = 1
    AUTHORIZED_STATUS = 2
    IN_ATTENDANCE_STATUS = 3
    CLOSED_STATUS = 4

    STATUS_CHOICES = (
        (OPEN_STATUS, 'Aberto / Não Autorizado'),
        (AUTHORIZED_STATUS, 'Autorizado'),
        (IN_ATTENDANCE_STATUS, 'Em atendimento'),
        (CLOSED_STATUS, 'Encerrado'),
    )

    user = models.ForeignKey(
        User, related_name='vehicle_requests', on_delete=models.SET_NULL, null=True)
    unit = models.ForeignKey(
        Unit, related_name='vehicle_requests_units', on_delete=models.SET_NULL, null=True)
    scheduled_date = models.DateField()
    request_type = models.IntegerField(choices=REQUEST_CHOICES)
    passanger_number = models.PositiveSmallIntegerField()
    destiny = models.CharField(max_length=80)
    purpose = models.TextField(max_length=250)
    status = models.IntegerField(choices=STATUS_CHOICES, default=OPEN_STATUS)
    authorized_by = models.ForeignKey(
        User, related_name='authorized_requests', on_delete=models.SET_NULL, null=True)
    authorized_date = models.DateTimeField(blank=True, null=True)
    departure_date = models.DateField(blank=True, null=True)
    arrival_date = models.DateField(blank=True, null=True)
    driver = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    obs = models.TextField(blank=True, null=True)

    objects = VehicleRequestManager()

    class Meta:

        ordering = ['-created']

    def __str__(self):
        return self.unit.name
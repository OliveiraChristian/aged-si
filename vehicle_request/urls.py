from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^request/create$', views.CreateVehicleRequest.as_view(), name='create'),
    url(r'^request/(?P<pk>\d+)/detail$', views.VehicleRequestDetail.as_view(), name='detail'),
    url(r'^request/(?P<pk>\d+)/delete$', views.VehicleRequestDelete.as_view(), name='delete'),
    url(r'^request/(?P<pk>\d+)/authorize$', views.authorize, name='authorize'),
    url(r'^request/(?P<pk>\d+)/attend$', views.attend, name='attend'),
    url(r'^request/(?P<pk>\d+)/finish$', views.finish, name='finish'),
    url(r'^request/(?P<pk>\d+)/pdf$', views.VehicleRequestPDF.as_view(), name='pdf'),
    url(r'^search/$', views.SearchVehicleRequestView.as_view(), name='search'),
]
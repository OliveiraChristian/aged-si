from django.contrib import admin
from .models import VehicleModel, Vehicle

# Register your models here.
class VehicleAdmin(admin.ModelAdmin):
    
    list_display = ['model', 'license_plate']

    class Meta:
        model = Vehicle

admin.site.register(Vehicle, VehicleAdmin)

class VehicleModelAdmin(admin.ModelAdmin):
    
    list_display = ['brand', 'model']

    class Meta:
        model = VehicleModel

admin.site.register(VehicleModel, VehicleModelAdmin)
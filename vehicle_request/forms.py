from django import forms
from django.contrib.auth.models import User

from . import models
from organization.models import Unit, UnitUser


class VehicleRequestForm(forms.ModelForm):

    class Meta:

        model = models.VehicleRequest
        fields = ['unit', 'scheduled_date', 'request_type',
                  'passanger_number', 'destiny', 'purpose']
        #exclude = ['user', 'status', 'authorized_date', 'authorized_by', 'departure_date', 'arrival_date', 'driver', 'vehicle', 'obs']
        labels = {
            'unit': ('Setor'),
            'scheduled_date': ('Data de reserva'),
            'request_type': ('Tipo de requisição'),
            'passanger_number': ('Número de passageiros'),
            'destiny': ('Destino'),
            'purpose': ('Objetivo'),
        }
        widgets = {
            'scheduled_date': forms.TextInput(attrs={'placeholder': 'DD/MM/AAAA'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user_units = Unit.objects.filter(pk__in=UnitUser.objects.filter(user=kwargs['initial']['user']).values('unit')).order_by('name')
        self.fields['unit'].queryset = user_units

from django.apps import AppConfig


class VehicleRequestConfig(AppConfig):
    name = 'vehicle_request'

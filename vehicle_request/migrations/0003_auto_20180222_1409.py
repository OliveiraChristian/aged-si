# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-22 17:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vehicle_request', '0002_auto_20180215_2132'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vehiclerequest',
            options={'ordering': ['-created']},
        ),
    ]
